def count(n):
    if n <= 1:
        return 1
    return sum(count(i) * count(n-1-i) for i in range(n))

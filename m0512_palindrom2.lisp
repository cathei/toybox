;def cut(s):
;    if s == s[::-1]:
;        return 0
;    return min(1 + cut(s[:i]) + cut(s[i:]) for i in range(1, len(s)))
;    
;print cut('aab')

(defun is-palindrom (s)
    (equal s (reverse s)))

(defun valid (a b)
    (if (and a b) (min a b)
        (or a b)))

(defun f (h s c) 
    (if (and (is-palindrom h) (is-palindrom s)) c
    (if (not s) nil
    (if (not h) (f (cons (car s) h) (cdr s) (1+ c))
    (if (is-palindrom h) (valid (f () s c) (f (cons (car s) h) (cdr s) c))
        (f (cons (car s) h) (cdr s) c))))))

(princ (f () '(a m a n a p l a n a c a n a l p a n a m a) 0))

